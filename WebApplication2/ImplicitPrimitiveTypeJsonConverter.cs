﻿using System;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using WebApplication2.Controllers;

namespace WebApplication2
{
    public class ImplicitPrimitiveTypeJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var x = value as IJsonConverablePrimitiveType;

            writer.WriteRawValue(x.JsonResult());

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            var correctInterface = objectType.GetInterfaces()
                                             .FirstOrDefault(x =>
                                                                 x.GenericTypeArguments.FirstOrDefault() ==
                                                                 objectType &&
                                                                 x.GetGenericTypeDefinition() ==
                                                                 typeof(IModelbinablePrimitiveType<>));

            var result = correctInterface.GetMethod(nameof(IModelbinablePrimitiveType<int>.ModelBind),
                                                    BindingFlags.Public | BindingFlags.Instance |
                                                    BindingFlags.InvokeMethod)
                                         .Invoke(Activator.CreateInstance(objectType),
                                                 new object[] {new[] {reader.Value.ToString()}});

            return result;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(IJsonConverablePrimitiveType).IsAssignableFrom(objectType);
        }
    }
}