﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using WebApplication2.Controllers;

namespace WebApplication2
{
    public class CustomPrimitiveTypeBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var correctInterface = context.Metadata.ModelType.GetInterfaces()
                                          .FirstOrDefault(x =>
                                                              x.GenericTypeArguments.FirstOrDefault() == context
                                                                  .Metadata.ModelType &&
                                                              x.GetGenericTypeDefinition() ==
                                                              typeof(IModelbinablePrimitiveType<>));

            if (correctInterface != null)
            {
                return new CustomPrimitiveModelBinder(correctInterface);
            }


            return null;
        }
    }

    public class CustomPrimitiveModelBinder : IModelBinder
    {
        private readonly Type _correctInterface;

        public CustomPrimitiveModelBinder(Type correctInterface)
        {
            _correctInterface = correctInterface;
        }

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            var values = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).Values.ToArray();

            var result = _correctInterface.GetMethod(nameof(IModelbinablePrimitiveType<int>.ModelBind),
                                                     BindingFlags.Public | BindingFlags.Instance |
                                                     BindingFlags.InvokeMethod)
                                          .Invoke(Activator.CreateInstance(bindingContext.ModelMetadata.ModelType),
                                                  new object[] {values});

            bindingContext.Result = ModelBindingResult.Success(result);


            return TaskCache.CompletedTask;
        }
    }
}