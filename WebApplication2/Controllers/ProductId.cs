﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace WebApplication2.Controllers
{
    public struct ProductId : IModelbinablePrimitiveType<ProductId>
    {
        private readonly int _value;
        private const int DefaultValue = 1;

        [JsonProperty]
        private int Value => _value == default(int) ? DefaultValue : _value;

        public ProductId(int value)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value),
                                                      $"The {nameof(ProductId)}-{nameof(value)} must be largen then zero.");
            }
            _value = value;
        }

        public static implicit operator ProductId(int value)
        {
            return new ProductId(value);
        }

        public static implicit operator int(ProductId productId)
        {
            return productId.Value;
        }

        public ProductId ModelBind(string[] values)
        {
            return new ProductId(int.Parse(values.First()));
        }

        public override string ToString()
        {
            return $"{nameof(ProductId)}: {Value}";
        }

        public string JsonResult()
        {
            return Value.ToString();
        }

        public bool Equals(ProductId other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(objA: null, objB: obj))
            {
                return false;
            }
            return obj is ProductId && Equals((ProductId) obj);
        }

        public override int GetHashCode()
        {
            return Value;
        }
    }
}