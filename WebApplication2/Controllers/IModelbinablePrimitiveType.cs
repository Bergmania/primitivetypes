using Newtonsoft.Json;

namespace WebApplication2.Controllers
{
    
    public interface IModelbinablePrimitiveType<T> : IJsonConverablePrimitiveType
    {
        T ModelBind(string[] values);
    }

    [JsonConverter(typeof(ImplicitPrimitiveTypeJsonConverter))]
    public interface IJsonConverablePrimitiveType
    {
        string JsonResult();
    }
}