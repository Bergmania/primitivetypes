﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication2.Controllers
{
    [Route(template: "api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new[] {"value1", "value2"};
        }

        // GET api/values/5
        [HttpGet(template: "{id}")]
        public string Get(ProductId id)
        {
            return "value " + id;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut(template: "{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete(template: "{id}")]
        public void Delete(int id)
        {
        }
    }
}