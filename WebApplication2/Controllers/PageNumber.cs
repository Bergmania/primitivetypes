﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace WebApplication2.Controllers
{
    public struct PageNumber : IModelbinablePrimitiveType<PageNumber>
    {
        private readonly int _value;
        private const int DefaultValue = 1;

        [JsonProperty]
        private int Value => _value == default(int) ? DefaultValue : _value;

        public PageNumber(int value)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value),
                                                      $"The {nameof(PageNumber)}-{nameof(value)} must be largen then zero.");
            }
            _value = value;
        }

        public static implicit operator PageNumber(int value)
        {
            return new PageNumber(value);
        }

        public static implicit operator int(PageNumber pageNumber)
        {
            return pageNumber.Value;
        }

        public PageNumber ModelBind(string[] values)
        {
            return new PageNumber(int.Parse(values.First()));
        }

        public override string ToString()
        {
            return $"{nameof(PageNumber)}: {Value}";
        }

        public string JsonResult()
        {
            return Value.ToString();
        }

        public bool Equals(PageNumber other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(objA: null, objB: obj))
            {
                return false;
            }
            return obj is PageNumber && Equals((PageNumber) obj);
        }

        public override int GetHashCode()
        {
            return Value;
        }
    }
}