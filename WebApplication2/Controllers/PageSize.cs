﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace WebApplication2.Controllers
{
    public struct PageSize : IModelbinablePrimitiveType<PageSize>
    {
        private readonly int _value;
        private const int DefaultValue = 20;

        [JsonProperty]
        private int Value => _value == default(int) ? DefaultValue : _value;

        public PageSize(int value)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value),
                                                      $"The {nameof(PageSize)}-{nameof(value)} must be largen then zero.");
            }
            _value = value;
        }

        public static implicit operator PageSize(int value)
        {
            return new PageSize(value);
        }

        public static implicit operator int(PageSize pageSize)
        {
            return pageSize.Value != 0 ? pageSize.Value : DefaultValue;
        }

        public PageSize ModelBind(string[] values)
        {
            return new PageSize(int.Parse(values.First()));
        }

        public override string ToString()
        {
            return $"{nameof(PageSize)}: {Value}";
        }

        public string JsonResult()
        {
            return Value.ToString();
        }

        public bool Equals(PageSize other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(objA: null, objB: obj))
            {
                return false;
            }
            return obj is PageSize && Equals((PageSize) obj);
        }

        public override int GetHashCode()
        {
            return Value;
        }
    }
}