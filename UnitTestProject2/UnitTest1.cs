using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using WebApplication2.Controllers;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Serialize_and_Deserialize_Object()
        {

            var dto = new TestDTO()
            {
                PageNumber = new PageNumber(),
                ProductId = new ProductId(),
                PageSize = new PageSize()
            };

            var serialized = JsonConvert.SerializeObject(dto);

            Console.WriteLine(serialized);

            var deserialized = JsonConvert.DeserializeObject<TestDTO>(serialized);

            Assert.AreEqual(dto, deserialized);

            
        }
    }

    public class TestDTO
    {
        public ProductId ProductId { get; set; }
        public PageNumber PageNumber { get; set; }
        public PageSize PageSize { get; set; }

        protected bool Equals(TestDTO other)
        {
            return ProductId.Equals(other.ProductId) && PageNumber.Equals(other.PageNumber) && PageSize.Equals(other.PageSize) ;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TestDTO) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = ProductId.GetHashCode();
                hashCode = (hashCode * 397) ^ PageNumber.GetHashCode();
                hashCode = (hashCode * 397) ^ PageSize.GetHashCode();
                return hashCode;
            }
        }
    }
}
